import './App.css';
import './SideNav.css'
import Navbar from './components/NavBar';
import SideNav from './components/SideNav'
 
import {
  BrowserRouter as Router,
  Routes ,
  Route,
  Link,
} from "react-router-dom";
import { RadioButtonUncheckedSharp } from '@material-ui/icons';

function App() {


const menuItems = [
  {title: "Home", url:"/home"},
  {title: "Contact", url:"/contanct"},
  {title: "Users", url:"/users"},
  {title: "Products", url:"/users"},
]


 
  return (
    <Router>
    <div className="App">

      <Navbar/>

      <SideNav   items={menuItems}/>
     
      <Routes >
      <Route path='/' element={<>home</>} />
         
        </Routes >

    </div>
    </Router>
  );
}

export default App;
